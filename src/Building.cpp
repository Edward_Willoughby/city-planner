/********************************************************************
*	Function definitions for the Building class.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "Building.h"

#include <DBEApp.h>
#include <DBETextureMgr.h>
/********************************************************************
*	Namespaces, defines, constants, and local variables.
********************************************************************/

static Building gs_optionList[] = {
	//			Name			Texture file name			Cost				Yield			Build time
	Building( "House",			"building_house",		Resources( 150),	Resources(  50),	1.0f),
	Building( "Gold Mine",		"building_gold_mine",	Resources( 450),	Resources( 200),	2.0f),
	Building( "Lumber Mill",	"building_lumber_mill",	Resources( 300),	Resources( 100),	1.5f),
};

static const u32 gsc_optionListCount( sizeof( gs_optionList) / sizeof( gs_optionList[0]));

/// The time it takes for a building to produce its yield and give it to the player.
static const float gsc_yeildTime( 5.0f);


/**
* Constructor.
*/
Building::Building( const char* name, const char* texture, Resources cost, Resources yield, float buildTime)
	: m_name( name)
	, m_textureName( texture)
	, m_cost( cost)
	, m_yield( yield)
	, m_buildTime( buildTime)
{}

/**
* Copy constructor.
*/
Building::Building( const Building& other) {
	m_name			= other.m_name;
	mp_texture		= other.mp_texture;
	m_cost			= other.m_cost;
	m_yield			= other.m_yield;
	m_buildTime		= other.m_buildTime;
	m_textureName	= other.m_textureName;
}

/**
* Destructor.
*/
Building::~Building() {

}

/**
* 
*
* @param :: 
*
* @return 
*/
bool Building::Init() {
	ID3D11SamplerState* sampler( GET_APP()->GetSamplerState( true, true, true));
	char buffer[256];

	for( u32 i( 0); i < gsc_optionListCount; ++i) {
		sprintf_s( buffer, 256, "%s%s.dds", "res/textures/", gs_optionList[i].m_textureName);
		gs_optionList[i].mp_texture = MGR_TEXTURE().LoadTexture( buffer, sampler);
	}

	return true;
}

void Building::Shutdown() {
	for( u32 i( 0); i < gsc_optionListCount; ++i)
		MGR_TEXTURE().DeleteTexture( gs_optionList[i].mp_texture);
}

u32 Building::GetBuildingCount() {
	return gsc_optionListCount;
}

Building* Building::GetBuilding( u32 i) {
	return &gs_optionList[i];
}

s32 Building::GetBuildingIndex( Building* building) {
	for( s32 i( 0); i < gsc_optionListCount; ++i) {
		if( building != &gs_optionList[i])
			continue;

		return i;
	}

	return -1;
}

float Building::GetYieldTime() {
	return gsc_yeildTime;
}