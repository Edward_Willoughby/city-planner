/********************************************************************
*
*	CLASS		:: EndLevelOverlay
*	DESCRIPTION	:: Display an overlay when the level is over, whether the player won or lost.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2015 / 04 / 03
*
********************************************************************/

#ifndef EndLevelOverlayH
#define EndLevelOverlayH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <DBEUITexture.h>

/********************************************************************
*	Defines and constants.
********************************************************************/


/*******************************************************************/
class EndLevelOverlay : public DBE::UITexture {
	public:
		/// Constructor.
		EndLevelOverlay();
		/// Destructor.
		~EndLevelOverlay();
		
		/// Updates the overlay.
		bool OnUpdate( float deltaTime);
		/// Renders the overlay.
		//void OnRender( float deltaTime);

		/// Resets the overlay.
		void Reset();
		
	private:
		float m_delay;
		float m_fadeDuration;

		float m_opacity;

		
		/// Private copy constructor to prevent accidental multiple instances.
		EndLevelOverlay( const EndLevelOverlay& other);
		/// Private assignment operator to prevent accidental multiple instances.
		EndLevelOverlay& operator=( const EndLevelOverlay& other);
		
};

/*******************************************************************/
#endif	// #ifndef EndLevelOverlayH
