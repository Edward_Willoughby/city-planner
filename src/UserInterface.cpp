/********************************************************************
*	Function definitions for the UserInterface class.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "UserInterface.h"

#include <DBEApp.h>
#include <DBETypes.h>
/********************************************************************
*	Namespaces, defines, constants, and local variables.
********************************************************************/
using namespace DBE;



/**
* Constructor.
*/
UserInterface::UserInterface() {

}

/**
* Destructor.
*/
UserInterface::~UserInterface() {

}

/**
* 
*
* @param :: 
*
* @return 
*/
bool UserInterface::Init() {
	m_optionList = OptionList::OL_None;
	mp_options = nullptr;
	m_optionCount = 0;
	m_optionHovered = -1;

	ID3D11SamplerState* sampler( GET_APP()->GetSamplerState( true));

	m_optionsBuildingsCount = Building::GetBuildingCount();
	mp_optionsBuildings = new UIOption[m_optionsBuildingsCount];
	for( u32 i( 0); i < Building::GetBuildingCount(); ++i) {
		mp_optionsBuildings[i].m_pos = Vec3( (0.12f * i) + 0.05f, -(1.0f / 5) * 2, 0.0f);
		mp_optionsBuildings[i].m_scale = Vec3( 0.1f, 0.1f, 1.0f);

		mp_optionsBuildings[i].mp_texture = Building::GetBuilding( i)->mp_texture;
	}
	mp_textureOptionBorder = MGR_TEXTURE().LoadTexture( "res/textures/building_option_border.dds", sampler);

	m_optionsWonCount = 1;
	mp_optionsWon = new UIOption[m_optionsWonCount];
	mp_optionsWon[0].m_pos = Vec3( 0.0f, -0.2f, -1.0f);
	mp_optionsWon[0].m_scale = Vec3( 0.2f, 0.1f, 1.0f);
	mp_optionsWon[0].mp_texture = MGR_TEXTURE().LoadTexture( "res/textures/button_quit.dds", sampler);

	m_optionsLostCount = 2;
	mp_optionsLost = new UIOption[m_optionsLostCount];
	mp_optionsLost[0].m_pos = Vec3( 0.0f - 0.15f, -0.2f, -1.0f);
	mp_optionsLost[0].m_scale = Vec3( 0.2f, 0.1f, 1.0f);
	mp_optionsLost[0].mp_texture = MGR_TEXTURE().LoadTexture( "res/textures/button_retry.dds", sampler);
	
	mp_optionsLost[1].m_pos = Vec3( 0.0f + 0.15f, -0.2f, -1.0f);
	mp_optionsLost[1].m_scale = Vec3( 0.2f, 0.1f, 1.0f);
	mp_optionsLost[1].mp_texture = MGR_TEXTURE().LoadTexture( "res/textures/button_quit.dds", sampler);


	return true;
}

/**
* 
*
* @param :: 
*
* @return 
*/
void UserInterface::Shutdown() {
	// This is so the UserInterface's base class doesn't try to delete the texture an additional
	// time.
	this->mp_texture = nullptr;

	SafeDeleteArray( mp_optionsBuildings);

	for( s32 i( 0); i < m_optionsWonCount; ++i)
		MGR_TEXTURE().DeleteTexture( mp_optionsWon[i].mp_texture);
	SafeDeleteArray( mp_optionsWon);

	for( s32 i( 0); i < m_optionsLostCount; ++i)
		MGR_TEXTURE().DeleteTexture( mp_optionsLost[i].mp_texture);
	SafeDeleteArray( mp_optionsLost);

	MGR_TEXTURE().DeleteTexture( mp_textureOptionBorder);
}

/**
* 
*
* @param :: 
*
* @return 
*/
bool UserInterface::OnUpdate( float deltaTime) {
	m_optionHovered = -1;

	if( mp_options == nullptr)
		return true;

	Vec2 pos;
	if( MGR_INPUT().GetMousePos( pos)) {
		if( pos.GetX() != -1 && pos.GetY() != -1) {
			//if( MGR_INPUT().KeyPressed( 'P'))
			//	m_optionCount = m_optionCount;

			// Convert the cursor position to screen coordinates.
			pos.SetX( (pos.GetX() / SCREEN_WIDTH) - 0.5f);
			pos.SetY( ((pos.GetY() / SCREEN_HEIGHT) - 0.5f) * -1.0f);

			for( s32 i( 0); i < m_optionCount; ++i) {
				if( !this->CursorIsOverOption( &mp_options[i], pos))
					continue;

				m_optionHovered = i;
				break;
			}
		}
	}
	
	return UITexture::OnUpdate( deltaTime);
}

/**
* 
*
* @param :: 
*
* @return 
*/
void UserInterface::OnRender( float deltaTime) {
	if( mp_options == nullptr)
		return;

	// Render the options' icons.
	for( s32 i( 0); i < m_optionCount; ++i) {
		this->MoveTo( mp_options[i].m_pos);
		this->ScaleTo( mp_options[i].m_scale);

		
		this->mp_texture = mp_options[i].mp_texture;

		GET_APP()->m_matWorld = this->GetWorldMatrix();

		UITexture::OnRender( deltaTime);
	}

	// Render the border if an option is being hovered over.
	if( m_optionList == OL_Buildings && m_optionHovered != -1) {
		this->MoveTo( mp_options[m_optionHovered].m_pos);
		this->MoveBy( 0.0f, 0.0f, -0.1f);
		this->ScaleTo( mp_options[m_optionHovered].m_scale);

		this->mp_texture = mp_textureOptionBorder;

		GET_APP()->m_matWorld = this->GetWorldMatrix();

		UITexture::OnRender( deltaTime);
	}
}

void UserInterface::Reset() {
	this->ShowOptions( OL_None);
}

/**
* 
*
* @param :: 
*
* @return 
*/
void UserInterface::ShowOptions( OptionList options) {
	m_optionList = options;
	m_optionHovered = -1;

	switch( options) {
		case OL_Buildings:
			mp_options = mp_optionsBuildings;
			m_optionCount = m_optionsBuildingsCount;
			break;

		case OL_LevelWon:
			mp_options = mp_optionsWon;
			m_optionCount = m_optionsWonCount;
			break;

		case OL_LevelLost:
			mp_options = mp_optionsLost;
			m_optionCount = m_optionsLostCount;
			break;

		case OL_None:
			mp_options = nullptr;
			m_optionCount = 0;
	}
}

/**
* 
*
* @param :: 
*
* @return 
*/
bool UserInterface::CursorIsOverOption( UIOption* option, const DBE::Vec2& mousePos) {
	float xMax( 0.0f), xMin( 0.0f);
	float yMax( 0.0f), yMin( 0.0f);
	float xMouse( mousePos.GetX()), yMouse( mousePos.GetY());

	xMax = (option->m_pos.GetX() + (option->m_scale.GetX() / 2));
	xMin = (option->m_pos.GetX() - (option->m_scale.GetX() / 2));
	yMax = (option->m_pos.GetY() + (option->m_scale.GetY() / 2));
	yMin = (option->m_pos.GetY() - (option->m_scale.GetY() / 2));

	if( xMouse >= xMin && xMouse <= xMax)
		if( yMouse >= yMin && yMouse <= yMax)
			return true;

	return false;
}