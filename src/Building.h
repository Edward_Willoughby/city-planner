/********************************************************************
*
*	CLASS		:: Building
*	DESCRIPTION	:: Holds data about a building (e.g. resources provided, cost, building time etc.).
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2015 / 03 / 24
*
********************************************************************/

#ifndef BuildingH
#define BuildingH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <DBETypes.h>

namespace DBE {
	struct Texture;
}
/********************************************************************
*	Defines and constants.
********************************************************************/
/**
* Contains the various resources in the game.
*/
struct Resources {
	Resources()
		: m_gold( 0)
	{}

	Resources( u32 g)
		: m_gold( g)
	{}

	Resources& operator+=( Resources& other) {
		m_gold += other.m_gold;
	}

	u32 m_gold;
};

/// The maximum amount of gold that the player can accumulate.
static const u32 MAX_GOLD(	10000);


/*******************************************************************/
class Building {
	public:
		/// Constructor.
		Building( const char* name, const char* texture, Resources cost, Resources yield, float buildTime);
		/// Copy constructor.
		Building( const Building& other);
		/// Destructor.
		~Building();

		static bool Init();
		static void Shutdown();
		static u32 GetBuildingCount();
		static Building* GetBuilding( u32 i);
		static s32 GetBuildingIndex( Building* building);
		static float GetYieldTime();

		const char*		m_name;
		DBE::Texture*	mp_texture;

		Resources m_cost;
		Resources m_yield;

		float m_buildTime;

		
	private:
		const char* m_textureName;
		
		/// Private assignment operator to prevent accidental multiple instances.
		Building& operator=( const Building& other);
		
};

/*******************************************************************/

/*******************************************************************/

#endif	// #ifndef BuildingH
