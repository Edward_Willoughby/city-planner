/********************************************************************
*
*	CLASS		:: UserInterface
*	DESCRIPTION	:: Orders and renders the interface.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2015 / 03 / 17
*
********************************************************************/

#ifndef UserInterfaceH
#define UserInterfaceH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <DBEMath.h>
#include <DBEUITexture.h>

#include "Building.h"
/********************************************************************
*	Defines and constants.
********************************************************************/
//static const char* gsc_optionsBuildingsTextures[] = {
//	"building_house",
//	"building_gold_mine",
//	"building_lumber_mill",
//};
//static const s32 gsc_countOptionsBuildings( sizeof( gsc_optionsBuildingsTextures) / sizeof( gsc_optionsBuildingsTextures[0]));

enum OptionList {
	OL_None = 0,
	OL_Buildings,
	OL_LevelWon,
	OL_LevelLost,
	OL_Count,
};


/*******************************************************************/
class UserInterface : public DBE::UITexture {
	public:
		/// Constructor.
		UserInterface();
		/// Destructor.
		~UserInterface();

		bool Init();
		void Shutdown();
		
		bool OnUpdate( float deltaTime);
		void OnRender( float deltaTime);

		void Reset();

		void ShowOptions( OptionList options);

		s32 GetSelectedOption() const {
			return m_optionHovered;
		}

	private:
		struct UIOption {
			DBE::Vec3 m_scale;
			DBE::Vec3 m_pos;

			DBE::Texture* mp_texture;
		};

		/// Tests whether the cursor is hovering over an option.
		bool CursorIsOverOption( UIOption* option, const DBE::Vec2& mousePos);

		OptionList m_optionList;

		UIOption*	mp_options;
		s32			m_optionCount;

		// Building options.
		UIOption*	mp_optionsBuildings;
		s32			m_optionsBuildingsCount;
		
		// Win screen options.
		UIOption*	mp_optionsWon;
		s32			m_optionsWonCount;

		// Loss screen options.
		UIOption*	mp_optionsLost;
		s32			m_optionsLostCount;
		
		DBE::Texture*	mp_textureOptionBorder;
		s32				m_optionHovered;

		
		/// Private copy constructor to prevent accidental multiple instances.
		UserInterface( const UserInterface& other);
		/// Private assignment operator to prevent accidental multiple instances.
		UserInterface& operator=( const UserInterface& other);
		
};

/*******************************************************************/
#endif	// #ifndef UserInterfaceH
