/********************************************************************
*
*	CLASS		:: Objective
*	DESCRIPTION	:: Holds a requirement for completing a level.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2015 / 04 / 01
*
********************************************************************/

#ifndef ObjectiveH
#define ObjectiveH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <DBETypes.h>

#include "Building.h"
/********************************************************************
*	Defines and constants.
********************************************************************/
enum ObjectiveType {
	OT_Invalid = 0,
	OT_Gold,
	OT_Building,
	OT_Count,
};


/*******************************************************************/
class Objective {
	public:
		/// Constructor.
		Objective();
		/// Destructor.
		~Objective();

		void SetRequirementBuilding( Building* building, s32 count);
		void SetRequirementResources( Resources* res);

		void TestRequirementBuilding( s32 count[]);
		void TestRequirementResources( Resources* res);
		
		void SetStatus( bool complete);
		bool IsComplete() const;

		ObjectiveType GetType() const;
		const char* GetDescription() const;
		
	private:
		bool m_completed;

		ObjectiveType m_type;

		char m_description[256];

		u32 m_building;
		s32 m_buildingCount;

		Resources m_resources;
		
		
		/// Private copy constructor to prevent accidental multiple instances.
		Objective( const Objective& other);
		/// Private assignment operator to prevent accidental multiple instances.
		Objective& operator=( const Objective& other);
		
};

/*******************************************************************/
#endif	// #ifndef ObjectiveH
