/********************************************************************
*	Function definitions for the DustEmitter class.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "DustEmitter.h"

#include <DBEApp.h>
#include <DBEColour.h>
#include <DBEGraphicsHelpers.h>
#include <DBEPixelShader.h>
#include <DBERandom.h>
#include <DBEShader.h>
#include <DBEUITexture.h>
#include <DBEVertexShader.h>
#include <DBEVertexTypes.h>
/********************************************************************
*	Namespaces, defines, constants, and local variables.
********************************************************************/
using namespace DBE;
using namespace DirectX;

DustParticleMesh* DustEmitter::mp_mesh = nullptr;

/// File name of the dust texture.
static const char* gsc_dustTextureName( "res/textures/dust.dds");

/// How long particles live for before dying.
static float gs_timeToDie( 1.0f);


/********************************************************************
*	DustParticle
********************************************************************/
/**
* Constructor.
*/
DustParticleMesh::DustParticleMesh() {
	this->SetTexture( gsc_dustTextureName, GET_APP()->GetSamplerState( true));

	if( !MGR_SHADER().GetShader<DustParticleVertexShader>( gsc_dustParticleVertexShaderName, mp_dustVS)) {
		mp_dustVS = new DustParticleVertexShader();
		mp_dustVS->Init();

		MGR_SHADER().AddShader<DustParticleVertexShader>( gsc_dustParticleVertexShaderName, mp_dustVS);
	}
	if( !MGR_SHADER().GetShader<DustParticlePixelShader>( gsc_dustParticlePixelShaderName, mp_dustPS)) {
		mp_dustPS = new DustParticlePixelShader();
		mp_dustPS->Init();

		MGR_SHADER().AddShader<DustParticlePixelShader>( gsc_dustParticlePixelShaderName, mp_dustPS);
	}
}

/**
* Destructor.
*/
DustParticleMesh::~DustParticleMesh() {
	MGR_SHADER().RemoveShader<DustParticleVertexShader>( mp_dustVS);
	MGR_SHADER().RemoveShader<DustParticlePixelShader>( mp_dustPS);
}

/**
* Handles some of the rendering that all subsequent particles will need.
*
* @param deltaTime :: The time taken to render the last frame.
*/
void DustParticleMesh::PreRender( float deltaTime) {
	ID3D11DeviceContext* p_context( GET_APP()->mp_D3DDeviceContext);

	mp_dustPS->PassTextureAndSampler( mp_texture->mp_textureView, mp_texture->mp_samplerState);

	// Set up the vertex and pixel shaders.
	p_context->VSSetShader( mp_dustVS->mp_VS, nullptr, 0);
	p_context->PSSetShader( mp_dustPS->mp_PS, nullptr, 0);

	p_context->IASetInputLayout( mp_dustVS->mp_IL);
	p_context->IASetPrimitiveTopology( D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	ID3D11Buffer* ap_vertBuffer[1]	= { this->GetVertexBuffer() };
	UINT aVertStride[1]				= { sizeof( VertPos3fColour4ubTex2f) };
	UINT aOffset[1]					= { 0 };
	p_context->IASetVertexBuffers( 0, 1, ap_vertBuffer, aVertStride, aOffset);

	p_context->IASetIndexBuffer( this->GetIndexBuffer(), DXGI_FORMAT_R32_UINT, 0);
}

/**
* Renders the particle (call 'Render').
*
* @param deltaTime :: The time taken to render the last frame.
*/
void DustParticleMesh::OnRender( float deltaTime) {
	ID3D11DeviceContext* p_context( GET_APP()->mp_D3DDeviceContext);

	mp_dustVS->PassVarsToCBuffer( &GET_APP()->GetWVP(), mp_texture->m_textureHeight, mp_texture->m_textureWidth);
	mp_dustPS->PassVarsToCBuffer( this->GetColour());

	// Set the constant buffers for the vertex and pixel shaders.
	if( mp_dustVS->mp_CBuffers[mp_dustVS->m_slotCBufferGlobalVars]) {
		ID3D11Buffer* p_constBuffers[] = { mp_dustVS->mp_CBuffers[mp_dustVS->m_slotCBufferGlobalVars] };
		p_context->VSSetConstantBuffers( mp_dustVS->m_slotCBufferGlobalVars, 1, p_constBuffers);
	}
	if( mp_dustPS->mp_CBuffers[mp_dustPS->m_slotCBufferGlobalVars]) {
		ID3D11Buffer* p_constBuffers[] = { mp_dustPS->mp_CBuffers[mp_dustPS->m_slotCBufferGlobalVars] };
		p_context->PSSetConstantBuffers( mp_dustPS->m_slotCBufferGlobalVars, 1, p_constBuffers);
	}

	p_context->DrawIndexed( this->GetIndexCount(), 0, 0);
}

/********************************************************************
*	DustEmitter
********************************************************************/
/**
* Initialise the particles.
*
* @return True if the particles were initialised correctly.
*/
bool DustEmitter::ParticlesInit() {
	if( mp_mesh == nullptr)
		mp_mesh = new DustParticleMesh();

	if( mp_mesh == nullptr)
		return false;

	return true;
}

/**
* Deletes the particles.
*/
void DustEmitter::ParticlesShutdown() {
	SafeDelete( mp_mesh);
}

/**
* Creates the particles. This is useful for when you want additional properties per particle; you
* can create an array of objects that derive from 'Particle'.
*
* @param particles		:: The pointer to the array of particles.
* @param maxParticles	:: The maximum number of particles that the emitter will hold.
*/
void DustEmitter::ParticlesCreateArray( DBE::Particle** particles, s32 maxParticles) {
	for( s32 i( 0); i < maxParticles; ++i)
		particles[i] = new DustParticle();
}

/**
* Event handler for when a particle spawns. This is called immediately after a particle has been
* created.
*
* @param particle :: The particle that just spawned.
*/
void DustEmitter::ParticleSpawn( DBE::Particle* particle) {
	DustParticle* p( dynamic_cast<DustParticle*>( particle));

	p->MoveTo( Random::Float( -0.5f, 0.5f), 0.0f, 0.0f);
	p->RotateTo( 0.0f, 0.0f, 0.0f);
	p->ScaleTo( Random::Float( 1.0f, 2.0f) / 2.0f);

	p->SetVelocity( (Random::Float( 1.0f) - 0.5f) / 10.0f, Random::Float( 1.0f, 2.0f) / 10.0f, 0.0f);

	p->m_rotSpeed = Vec3( 0.0f, 0.0f, DBE_ToRadians( 1.0f));

	p->SetColour( WHITE);
}

/**
* Update a particle.
*
* @param deltaTime	:: The time taken to render the last frame.
* @param particle	:: The particle to update.
*/
void DustEmitter::ParticleUpdate( float deltaTime, DBE::Particle* particle) {
	DustParticle* p( dynamic_cast<DustParticle*>( particle));

	p->m_age += deltaTime;
	
	Vec3 vel( p->GetVelocity());
	p->MoveBy( vel.GetX() * (deltaTime*10), vel.GetY() * (deltaTime*10), vel.GetZ() * (deltaTime*10));

	p->RotateBy( p->m_rotSpeed);

	// If it's past middle-aged then begin fading away.
	float timeToFade( gs_timeToDie / 3);
	if( p->m_age >= (gs_timeToDie - timeToFade)) {
		u32 colour( p->GetColour());
		u8 c[4];
		ColourToInts( c[0], c[1], c[2], c[3], colour);

		float energy( p->m_age - (gs_timeToDie - timeToFade));
		c[3] = (u8)(Lerp( 1.0f, 0.0f, energy / timeToFade) * 255.0f);

		p->SetColour( NumbersToColour( c[0], c[1], c[2], c[3]));
	}

	p->Update( deltaTime);
}

/**
* Pre-rendering that all particles will require. This isn't necessary, but it will speed up
* rendering times.
*
* @param deltaTime :: The time taken to render the last frame.
*/
void DustEmitter::ParticlePreRender( float deltaTime) {
	GET_APP()->SetBlendState( true);
	GET_APP()->SetDepthStencilState( true, false);
	GET_APP()->SetRasteriserState( true, false);

	mp_mesh->PreRender( deltaTime);
}

/**
* Render a particle.
*
* @param deltaTime		:: The time taken to render the last frame.
* @param particle		:: The particle to render.
* @param parentWorld	:: The particle's parent matrix (i.e. the emitter's world matrix).
*/
void DustEmitter::ParticleRender( float deltaTime, DBE::Particle* particle, const DBE::Matrix4& parentWorld) {
	mp_mesh->MoveTo( particle->GetPosition());
	mp_mesh->RotateTo( particle->m_billBoardRotation + particle->GetRotation());
	mp_mesh->ScaleTo( particle->GetScale());
	mp_mesh->SetVelocity( particle->GetVelocity());

	mp_mesh->SetColour( particle->GetColour());

	mp_mesh->Render( deltaTime, parentWorld);
}

/**
* Test if a particle has expired.
*
* @param particle :: The particle to test.
*
* @return True if the particle should be deleted.
*/
bool DustEmitter::ParticleIsDead( DBE::Particle* particle) {
	if( particle->m_age >= gs_timeToDie)
		return true;

	return false;
}

/**
* Event handler for when a particle dies.
*
* @param particle :: The particle to kill.
*/
void DustEmitter::ParticleDeath( DBE::Particle* particle) {
	particle->m_age = 0;
}

/**
* Create a random velocity value.
*
* @return A random velocity value.
*/
float DustEmitter::RandomVelocity() {
	float v( 1.0f);

	float res( Random::Float( 0.0f, v) - (v / 2));

	return res / 5.0f;
}