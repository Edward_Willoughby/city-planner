/********************************************************************
*	Function definitions for the Objective class.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "Objective.h"

#include <stdio.h>
/********************************************************************
*	Namespaces, defines, constants, and local variables.
********************************************************************/


/**
* Constructor.
*/
Objective::Objective()
	: m_completed( false)
	, m_type( OT_Invalid)
	, m_building( 0)
	, m_buildingCount( 0)
{
	m_description[0] = 0;
}

/**
* Destructor.
*/
Objective::~Objective() {
	
}

/**
* 
*
* @param :: 
*/
void Objective::SetRequirementBuilding( Building* building, s32 count) {
	m_type = OT_Building;

	s32 index( Building::GetBuildingIndex( building));
	if( index < 0)
		return;

	m_building = index;
	m_buildingCount = count;

	sprintf_s( m_description, 256, "Build %i %s%s.", m_buildingCount, building->m_name, m_buildingCount > 1 ? "s" : "");
}

/**
* 
*
* @param :: 
*/
void Objective::SetRequirementResources( Resources* res) {
	m_type = OT_Gold;

	m_resources = *res;

	sprintf_s( m_description, 256, "Gather %i gold.", m_resources.m_gold);
}

/**
* 
*
* @param :: 
*/
void Objective::TestRequirementBuilding( s32 count[]) {
	if( m_type != OT_Building)
		return;

	if( count[m_building] >= m_buildingCount)
		m_completed = true;
	else
		m_completed = false;
}

/**
* 
*
* @param :: 
*/
void Objective::TestRequirementResources( Resources* res) {
	if( m_type != OT_Gold)
		return;

	if( res->m_gold >= m_resources.m_gold)
		m_completed = true;
	else
		m_completed = false;
}

/**
* 
*
* @param :: 
*/
void Objective::SetStatus( bool complete) {
	m_completed = complete;
}

/**
* 
*
* @return 
*/
bool Objective::IsComplete() const {
	return m_completed;
}

/**
* 
*
* @return 
*/
ObjectiveType Objective::GetType() const {
	return m_type;
}

/**
* 
*
* @return 
*/
const char* Objective::GetDescription() const {
	return m_description;
}