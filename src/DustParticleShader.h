/********************************************************************
*
*	CLASS		:: DustParticleShader
*	DESCRIPTION	:: The vertex and pixel shader for the 'DustEmitter'.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2015 / 03 / 28
*
********************************************************************/

#ifndef DustParticleShaderH
#define DustParticleShaderH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <DBEPixelShader.h>
#include <DBETypes.h>
#include <DBEVertexShader.h>

namespace DirectX {
	struct XMMATRIX;
}
struct ID3D11SamplerState;
struct ID3D11ShaderResourceView;
/********************************************************************
*	Defines and constants.
********************************************************************/
/// The unique name for the sparkle particle vertex shader so it can be found in the shader manager.
static const char* gsc_dustParticleVertexShaderName = "DustParticleVertexShader";
/// The unique name for the sparkle particle pixel shader so it can be found in the shader manager.
static const char* gsc_dustParticlePixelShaderName = "DustParticlePixelShader";


/*******************************************************************/
class DustParticleVertexShader : public DBE::VertexShader {
	public:
		/// Constructor.
		DustParticleVertexShader();
		/// Destructor.
		~DustParticleVertexShader();

		bool Init();
		bool PassVarsToCBuffer( DBE::Matrix4* wvp, s32 imageHeight, s32 imageWidth);
		
		s32 m_slotCBufferGlobalVars;
		s32 m_slotWVP;
		s32 m_slotImageHeight;
		s32 m_slotImageWidth;
		
	private:
		
		
		/// Private copy constructor to prevent accidental multiple instances.
		DustParticleVertexShader( const DustParticleVertexShader& other);
		/// Private assignment operator to prevent accidental multiple instances.
		DustParticleVertexShader& operator=( const DustParticleVertexShader& other);
		
};

/*******************************************************************/
class DustParticlePixelShader : public DBE::PixelShader {
	public:
		/// Constructor.
		DustParticlePixelShader();
		/// Destructor.
		~DustParticlePixelShader();
		
		bool Init();
		bool PassVarsToCBuffer( u32 colour);
		bool PassTextureAndSampler( ID3D11ShaderResourceView* p_texture, ID3D11SamplerState* p_sampler);

		s32 m_slotCBufferGlobalVars;
		s32 m_slotColour;
		
		s32 m_slotTexture;
		s32 m_slotSampler;
		
	private:
		
		
		/// Private copy constructor to prevent accidental multiple instances.
		DustParticlePixelShader( const DustParticlePixelShader& other);
		/// Private assignment operator to prevent accidental multiple instances.
		DustParticlePixelShader& operator=( const DustParticlePixelShader& other);
		
};

/*******************************************************************/
#endif	// #ifndef DustParticleShaderH
