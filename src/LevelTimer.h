/********************************************************************
*
*	CLASS		:: LevelTimer
*	DESCRIPTION	:: Timer used on levels to determine how well the player did.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2015 / 04 / 04
*
********************************************************************/

#ifndef LevelTimerH
#define LevelTimerH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <DBEUITexture.h>

/********************************************************************
*	Defines and constants.
********************************************************************/
enum TimeBonus {
	TB_One = 0,
	TB_Two,
	TB_Three,
	TB_Count,
	TB_None,
};


/*******************************************************************/
class LevelTimer : public DBE::UITexture {
	public:
		/// Constructor.
		LevelTimer();
		/// Destructor.
		~LevelTimer();

		bool Init();
		void Shutdown();
		
		bool OnUpdate( float deltaTime);
		void OnRender( float deltaTime);

		void Set( float totalTime, float bonusTimes[]);

		bool TimeIsUp() const;
		TimeBonus GetBonus() const;
		float GetBonusTime( TimeBonus t) const;

		void Start();
		void Pause();
		void Reset();
		
	private:
		bool m_running;

		float m_totalTime;
		float m_timePast;
		float m_bonusTimes[TB_Count];

		DBE::Texture* mp_textureBackground;
		DBE::Texture* mp_textureBar;
		DBE::Texture* mp_textureBonusActive;
		DBE::Texture* mp_textureBonusInactive;
		
		/// Private copy constructor to prevent accidental multiple instances.
		LevelTimer( const LevelTimer& other);
		/// Private assignment operator to prevent accidental multiple instances.
		LevelTimer& operator=( const LevelTimer& other);
		
};

/*******************************************************************/
#endif	// #ifndef LevelTimerH
