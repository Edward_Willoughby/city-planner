/********************************************************************
*	Function definitions for the LevelTimer class.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "LevelTimer.h"

#include <DBEApp.h>
#include <DBEPixelShader.h>
#include <DBETypes.h>
#include <DBEUtilities.h>
#include <DBEVertexShader.h>
/********************************************************************
*	Namespaces, defines, constants, and local variables.
********************************************************************/
using namespace DBE;


/**
* Constructor.
*/
LevelTimer::LevelTimer()
	: m_running( false)
	, m_totalTime( 0.0f)
	, m_timePast( 0.0f)
{}

/**
* Destructor.
*/
LevelTimer::~LevelTimer() {
}

bool LevelTimer::Init() {
	ID3D11SamplerState* sampler( GET_APP()->GetSamplerState( true, true, true));

	mp_textureBackground	= MGR_TEXTURE().LoadTexture( "res/textures/timer_background.dds", sampler);
	mp_textureBar			= MGR_TEXTURE().LoadTexture( "res/textures/timer_background.dds", sampler);
	mp_textureBonusActive	= MGR_TEXTURE().LoadTexture( "res/textures/timer_bonus_indicator_active.dds", sampler);
	mp_textureBonusInactive	= MGR_TEXTURE().LoadTexture( "res/textures/timer_bonus_indicator_inactive.dds", sampler);

	return true;
}

void LevelTimer::Shutdown() {
	mp_texture = nullptr;

	MGR_TEXTURE().DeleteTexture( mp_textureBackground);
	MGR_TEXTURE().DeleteTexture( mp_textureBar);
	MGR_TEXTURE().DeleteTexture( mp_textureBonusActive);
	MGR_TEXTURE().DeleteTexture( mp_textureBonusInactive);
}

/**
* 
*
* @param :: 
*
* @return 
*/
bool LevelTimer::OnUpdate( float deltaTime) {
	if( !m_running)
		return true;

	m_timePast += deltaTime;

	return true;
}

/**
* 
*
* @param :: 
*/
void LevelTimer::OnRender( float deltaTime) {
	// Don't try to render if the vertex or index buffers are invalid, the shader isn't loaded, or
	// if there are too few polygons.
	//if( mp_vertBuffer == nullptr || mp_indexBuffer == nullptr || mp_VS == nullptr || mp_PS == nullptr || m_indexCount < 3)
	//	return;

	// Render the background bar.
	mp_texture = mp_textureBackground;
	this->SetColour( WHITE);
	this->MoveTo( 0.0f, 0.45f, 0.0f);
	this->ScaleTo( 0.21f, 0.05f, 1.0f);
	GET_APP()->m_matWorld = this->GetWorldMatrix();
	UITexture::OnRender( deltaTime);

	// Render the time remaining bar.
	mp_texture = mp_textureBar;
	this->SetColour( BLACK);
	float xScale( 0.2f * ((m_totalTime - m_timePast) / m_totalTime));
	float xPos( (xScale / 2.0f) - (0.2f / 2.0f));
	this->MoveTo( xPos, 0.45f, -0.1f);
	this->ScaleTo( xScale, 0.04f, 1.0f);
	GET_APP()->m_matWorld = this->GetWorldMatrix();
	UITexture::OnRender( deltaTime);


	// Render the three 'bonus time indicators'.
	this->SetColour( WHITE);
	float xPosStart( xPos - (xScale / 2.0f));
	float yPos( this->GetPosition().GetY() - this->GetScale().GetY() + 0.005f);
	float xWidth( 0.2f);
	for( s32 i( 0); i < TB_Count; ++i) {
		if( m_bonusTimes[i] >  m_timePast)
			mp_texture = mp_textureBonusActive;
		else
			mp_texture = mp_textureBonusInactive;

		float xOffset( xWidth * ((m_totalTime - m_bonusTimes[i]) / m_totalTime));
		xOffset += xPosStart;

		this->MoveTo( xOffset, yPos, -0.1f);
		this->ScaleTo( 0.03f, 0.03f, 1.0f);
		GET_APP()->m_matWorld = this->GetWorldMatrix();
		UITexture::OnRender( deltaTime);
	}
}

/**
* 
*
* @param :: 
*/
void LevelTimer::Set( float totalTime, float bonusTimes[]) {
	m_timePast = 0.0f;
	m_totalTime = totalTime;

	for( s32 i( 0); i < TB_Count; ++i)
		m_bonusTimes[i] = bonusTimes[i];
}

/**
* 
*
* @return 
*/
bool LevelTimer::TimeIsUp() const {
	if( m_timePast >= m_totalTime)
		return true;

	return false;
}

/**
* 
*
* @return 
*/
TimeBonus LevelTimer::GetBonus() const {
	for( s32 i( 0); i < TB_Count; ++i)
		if( m_timePast < m_bonusTimes[i])
			return TimeBonus( i);

	return TB_None;
}

/**
* 
*
* @param :: 
*
* @return 
*/
float LevelTimer::GetBonusTime( TimeBonus t) const {
	if( t < TB_One || t >= TB_Count)
		return 0.0f;

	return m_bonusTimes[t];
}

/**
* 
*/
void LevelTimer::Start() {
	m_running = true;
}

/**
* 
*/
void LevelTimer::Pause() {
	m_running = false;
}

/**
* 
*/
void LevelTimer::Reset() {
	m_running = false;
	m_timePast = 0.0f;
}