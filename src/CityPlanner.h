/********************************************************************
*
*	CLASS		:: CityPlanner
*	DESCRIPTION	:: A small game where you have to build a city to gain money to build a larger city.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2015 / 03 / 09
*
********************************************************************/

#ifndef CityPlannerH
#define CityPlannerH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <DBEApp.h>
#include <DBEColour.h>
#include <DBEMath.h>

#include "Level.h"
#include "LevelTimer.h"
#include "UserInterface.h"

namespace DBE {
	class Camera;
	class Font;
}
class EndLevelOverlay;
/********************************************************************
*	Defines and constants.
********************************************************************/
enum GameState {
	//GS_Init = 0,
	GS_Menu = 0,
	GS_Play,
	GS_Pause,
	GS_LevelComplete,
	GS_LevelFailed,
	GS_Count,
};


/*******************************************************************/
class CityPlanner : public DBE::App {
	public:
		/// Constructor.
		CityPlanner() {}

		/// Deals with initialisation.
		bool HandleInit();
		/// Deals with any input.
		bool HandleInput();
		/// Deals with any updating.
		bool HandleUpdate( float deltaTime);
		/// Deals with any rendering.
		bool HandleRender( float deltaTime);
		/// Deals with any 2D rendering.
		void HandleRender2D( float deltaTime);
		/// Deals with any 3D rendering.
		void HandleRender3D( float deltaTime);
		/// Deals with anything that needs to be released or shutdown.
		bool HandleShutdown();

	private:
		const char* m_windowTitle;

		DBE::Camera* mp_cam;

		DBE::Font*		mp_UIFont;
		Level*			mp_level;
		LevelTimer*		mp_levelTimer;
		UserInterface*	mp_ui;

		EndLevelOverlay* mp_overlay;

		GameState m_state;

		bool m_wireframe;

		/// Private copy constructor to prevent multiple instances.
		CityPlanner( const CityPlanner&);
		/// Private assignment operator to prevent multiple instances.
		CityPlanner& operator=( const CityPlanner&);

};

APP_MAIN( CityPlanner, BLUE);

/*******************************************************************/
#endif	// #ifndef CityPlannerH