/********************************************************************
*	Function definitions for the Level class.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "Level.h"

#include <cstdio>
#include <list>

#include <DBEApp.h>
#include <DBEBasicMesh.h>
#include <DBEBasicPixelShader.h>
#include <DBEBasicVertexShader.h>
#include <DBECamera.h>
#include <DBECollision.h>
#include <DBEGraphicsHelpers.h>
#include <DBEMath.h>
#include <DBERenderable.h>
#include <DBEShaderMgr.h>
#include <DBETextureMgr.h>
#include <DBEVertexTypes.h>

#include "LevelTimer.h"
/********************************************************************
*	Namespaces, defines, constants, and local variables.
********************************************************************/
using namespace DBE;
using namespace DirectX;

/// The name of level files, excluding their number.
static const char* gs_levelFile = "res/levels/level";
/// The size a level can be (in either dimension).
static s32 gsc_levelDimension( 15);


/**
* Constructor.
*/
Level::Level()
	: m_initialised( false)
	, m_resources( 0)
	, mp_objectives( nullptr)
	, m_objectiveCount( 0)
	, mp_data( nullptr)
	, mp_dataPlots( nullptr)
	, mp_hovered( nullptr)
	, mp_dust( nullptr)
{
	this->RotateTo( DBE_ToRadians( 90.0f), 0.0f, 0.0f);

	ID3D11SamplerState* sampler( GET_APP()->GetSamplerState( true, true, true));
	mp_invalid = MGR_TEXTURE().LoadTexture( "res/textures/missing.dds", sampler);
	mp_grass = MGR_TEXTURE().LoadTexture( "res/textures/moss.dds", sampler);
	mp_road = MGR_TEXTURE().LoadTexture( "res/textures/asphalt.dds", sampler);
	mp_plot = MGR_TEXTURE().LoadTexture( "res/textures/plot.dds", sampler);

	if( !MGR_SHADER().GetShader<LevelVertexShader>( gsc_levelVertexShaderName, mp_tileVS)) {
		mp_tileVS = new LevelVertexShader();
		mp_tileVS->Init();
		MGR_SHADER().AddShader<LevelVertexShader>( gsc_levelVertexShaderName, mp_tileVS);
	}
	if( !MGR_SHADER().GetShader<LevelPixelShader>( gsc_levelPixelShaderName, mp_tilePS)) {
		mp_tilePS = new LevelPixelShader();
		mp_tilePS->Init();
		MGR_SHADER().AddShader<LevelPixelShader>( gsc_levelPixelShaderName, mp_tilePS);
	}

	m_highlightMin = 0.0f;
	m_highlightMax = 1.0f;
	m_highlightInc = 0.01f;
	m_highlightSelected = 0.5f;

	this->CreatePlane();

	mp_dust = new Emitter<DustEmitter>();
	mp_dust->Init( 30, 5.0f);
}

/**
* Destructor.
*/
Level::~Level() {
	MGR_TEXTURE().DeleteTexture( mp_invalid);
	MGR_TEXTURE().DeleteTexture( mp_grass);
	MGR_TEXTURE().DeleteTexture( mp_road);
	MGR_TEXTURE().DeleteTexture( mp_plot);

	MGR_SHADER().RemoveShader<LevelVertexShader>( mp_tileVS);
	MGR_SHADER().RemoveShader<LevelPixelShader>( mp_tilePS);

	SafeDelete( mp_dust);
}

/**
* Updates the level.
*/
bool Level::OnUpdate( float deltaTime) {
	if( !m_initialised)
		return false;

	if( this->LevelIsOver())
		return true;

	this->FindAndUpdateHoveredTile();

	this->GenerateResources( deltaTime);

	this->UpdateObjectives();

	mp_levelTimer->Update( deltaTime);

	mp_dust->BillboardParticles( mp_activeCam->m_position);
	mp_dust->Update( deltaTime);

	return true;
}

/**
* Renders the level.
*/
void Level::OnRender( float deltaTime) {
	if( !m_initialised)
		return;

	// Don't try to render if the vertex or index buffers are invalid, the shader isn't loaded, or
	// if there are too few polygons.
	if( mp_vertBuffer == nullptr || mp_indexBuffer == nullptr || mp_tileVS->mp_VS == nullptr || mp_tilePS->mp_PS == nullptr || m_indexCount < 3)
		return;

	// Set the data that is the same for ALL tiles; vertex shader, pixel shader, input layout,
	// vertex buffer, index buffer etc...
	ID3D11DeviceContext* p_context( GET_APP()->mp_D3DDeviceContext);
	
	p_context->VSSetShader( mp_tileVS->mp_VS, nullptr, 0);
	p_context->PSSetShader( mp_tilePS->mp_PS, nullptr, 0);

	p_context->IASetInputLayout( mp_tileVS->mp_IL);
	p_context->IASetPrimitiveTopology( D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);

	ID3D11Buffer* ap_vertBuffer[1]	= { mp_vertBuffer };
	UINT aVertStride[1]				= { sizeof( VertPos3fColour4ubNormal3fTex2f) };
	UINT aOffset[1]					= { 0 };
	p_context->IASetVertexBuffers( 0, 1, ap_vertBuffer, aVertStride, aOffset);
	p_context->IASetIndexBuffer( mp_indexBuffer, DXGI_FORMAT_R32_UINT, 0);

	if( mp_tileVS->mp_CBuffers[mp_tileVS->m_slotCBufferGlobalVars]) {
		ID3D11Buffer* p_constBuffers[] = { mp_tileVS->mp_CBuffers[mp_tileVS->m_slotCBufferGlobalVars] };
		p_context->VSSetConstantBuffers( mp_tileVS->m_slotCBufferGlobalVars, 1, p_constBuffers);
	}
	if( mp_tilePS->mp_CBuffers[mp_tilePS->m_slotCBufferGlobalVars]) {
		ID3D11Buffer* p_constBuffers[] = { mp_tilePS->mp_CBuffers[mp_tilePS->m_slotCBufferGlobalVars] };
		p_context->PSSetConstantBuffers( mp_tilePS->m_slotCBufferGlobalVars, 1, p_constBuffers);
	}

	std::list<TileData*> plots;

	// ... Render the tiles in groups based on the texture they use.
	for( s32 tc( 0); tc < TC_Count; ++tc) {
		// Set the texture...
		switch( tc) {
			case TC_Invalid:	mp_tilePS->SetTexture( mp_invalid);	break;
			case TC_Grass:		mp_tilePS->SetTexture( mp_grass);	break;
			case TC_Road:		mp_tilePS->SetTexture( mp_road);	break;
			case TC_Plot:		/*mp_tilePS->SetTexture( mp_plot);*/	break;
			default:			continue;
		}

		// ... And now render each tile ensuring the world matrix and colour is changed for each one.
		for( s32 i( 0); i < gsc_levelDimension; ++i) {
			for( s32 j( 0); j < gsc_levelDimension; ++j) {
				// Skip this tile for now if it doesn't match the texture.
				if( mp_data[i][j].m_code != (TexelCode)tc)
					continue;

				// If it's a plot, then store the tile to render later (they don't necessarily all
				// share the same texture).
				if( mp_data[i][j].m_code == TC_Plot) {
					plots.push_back( &mp_data[i][j]);
					continue;
				}

				this->MoveTo( mp_data[i][j].m_offset);

				GET_APP()->m_matWorld = this->GetWorldMatrix( true);

				mp_tileVS->SetVars( &GET_APP()->m_matWorld);
				mp_tilePS->SetVars( &mp_data[i][j].m_highlight);

				p_context->DrawIndexed( m_indexCount, 0, 0);
			}
		}
	}

	// Now render the plots.
	for( s32 i( -1); i < (s32)Building::GetBuildingCount(); ++i) {
		if( i == -1)
			mp_tilePS->SetTexture( mp_plot);
		else
			mp_tilePS->SetTexture( Building::GetBuilding( i)->mp_texture);

		for( std::list<TileData*>::iterator it( plots.begin()); it != plots.end(); ) {
			s32 option( (*it)->m_buildingOption);
			// If the building is currently in development then apply the normal plot texture.
			if( (*it)->m_buildingTime > 0.0f && !(*it)->m_built)
				option = -1;

			// Only render the tile if it's building option is the currently selected one.
			if( option != i) {
				++it;
				continue;
			}

			this->MoveTo( (*it)->m_offset);

			GET_APP()->m_matWorld = this->GetWorldMatrix( true);

			mp_tileVS->SetVars( &GET_APP()->m_matWorld);
			mp_tilePS->SetVars( &(*it)->m_highlight);

			p_context->DrawIndexed( m_indexCount, 0, 0);

			it = plots.erase( it);
		}
	}

	// Render the dust particles if a building is being built.
	for( s32 i( 0); i < m_plotCount; ++i) {
		if( mp_dataPlots[i]->m_built || mp_dataPlots[i]->m_buildingOption == -1)
			continue;

		mp_dust->MoveTo( mp_dataPlots[i]->m_offset + Vec3( 0.0f, 0.0f, -0.25f));
		mp_dust->Render( deltaTime);
	}
}

/**
* 
*
* @param :: 
*
* @return 
*/
bool Level::LoadLevel( s32 l, DBE::Camera* cam, LevelTimer* timer) {
	// This needs to be put somewhere else (possibly in an 'Init' function) but it'll suffice for
	// now.
	mp_activeCam = cam;
	mp_levelTimer = timer;

	// Create the file name based on the level number.
	char fileName[256];
	sprintf_s( fileName, 256, "%s%s%i%s", gs_levelFile, l < 10 ? "0" : "", l, ".bmp");
	if( !this->CreateLevelFromFile( fileName))
		return false;

	this->Reset();

	m_initialised = true;

	return true;
}

/**
* Deletes an existing level.
*/
void Level::DeleteLevel() {
	// Delete the level.
	if( mp_data != nullptr) {
		SafeDelete( mp_dataPlots);

		for( s32 i( 0); i < gsc_levelDimension; ++i)
			SafeDeleteArray( mp_data[i]);

		SafeDeleteArray( mp_data);
	}

	// Delete the objectives.
	SafeDeleteArray( mp_objectives);
	m_objectiveCount = 0;

	m_initialised = false;
}

/**
* Resets the level to be started again.
*/
void Level::Reset() {
	// Reset all of the plots.
	for( s32 i( 0); i < m_plotCount; ++i) {
		mp_dataPlots[i]->m_buildingOption = -1;
		mp_dataPlots[i]->m_built = false;
	}

	// Set the player's initial gold.
	m_resources.m_gold = 1000;

	// Set the level timer.
	float bonuses[TB_Count] = { 20.0f, 30.0f, 40.0f };
	mp_levelTimer->Set( 50.0f, bonuses);

	// Create the objectives.
	// This is hard-coded for now, but will eventually be part of the 'level file'.
	m_objectiveCount = 3;
	mp_objectives = new Objective[m_objectiveCount];

	mp_objectives[0].SetRequirementResources( &Resources( 1000));				// 1000 gold.
	mp_objectives[1].SetRequirementBuilding( Building::GetBuilding( 0), 3);		// 3 houses.
	mp_objectives[2].SetRequirementBuilding( Building::GetBuilding( 1), 1);		// 1 gold mine.
}

/**
* Checks if the player has won this level.
*/
bool Level::LevelIsWon() const {
	for( s32 i( 0); i < m_objectiveCount; ++i)
		if( !mp_objectives[i].IsComplete())
			return false;

	return true;
}

/**
* Checks if the player has lost this level.
*/
bool Level::LevelIsLost() const {
	if( mp_levelTimer->TimeIsUp())
		return true;

	return false;
}

/**
* Check if a win OR loss condition has been met.
*/
bool Level::LevelIsOver() const {
	if( this->LevelIsWon() || this->LevelIsLost())
		return true;

	return false;
}

/**
* Gets the number of objectives for this level.
*/
s32 Level::GetObjectiveCount() const {
	return m_objectiveCount;
}

/**
* Gets an objective.
*/
Objective* Level::GetObjective( s32 index) const {
	if( index < 0 || index >= m_objectiveCount)
		return nullptr;

	return &mp_objectives[index];
}

void Level::AddGold( u32 g) {
	m_resources.m_gold += g;

	m_resources.m_gold = Clamp<u32>( m_resources.m_gold, 0, MAX_GOLD);
}

void Level::LoseGold( u32 g) {
	m_resources.m_gold -= g;

	m_resources.m_gold = Clamp<u32>( m_resources.m_gold, 0, MAX_GOLD);
}

u32 Level::GetGold() const {
	return m_resources.m_gold;
}

void Level::PickTile() {
	if( !m_initialised)
		return;

	if( mp_hovered == nullptr)
		return;

	// Reset the old picked tile's highlight.
	if( mp_picked != nullptr)
		mp_picked->m_highlight = m_highlightMin;

	// Set the new picked tile and its highlight.
	mp_picked = mp_hovered;
	mp_picked->m_highlight = m_highlightSelected;
}

void Level::DeselectTile() {
	if( !m_initialised)
		return;

	if( mp_picked == nullptr)
		return;

	mp_picked->m_highlight = m_highlightMin;
	mp_picked = nullptr;
}

bool Level::CanAffordBuilding( u32 option) {
	if( !m_initialised)
		return false;

	Resources cost( Building::GetBuilding( option)->m_cost);

	if( m_resources.m_gold < cost.m_gold)
		return false;

	return true;
}

void Level::SelectBuilding( u32 option) {
	if( !m_initialised)
		return;

	mp_picked->m_buildingOption = option;
	mp_picked->m_buildingTime = 0.0f;

	Resources cost( Building::GetBuilding( mp_picked->m_buildingOption)->m_cost);
	this->LoseGold( cost.m_gold);

	this->DeselectTile();
}

s32 Level::GetHoveredTile() const {
	if( !m_initialised)
		return -1;

	if( mp_hovered == nullptr)
		return -1;

	for( s32 i( 0); i < 16; ++i) {
		for( s32 j( 0); j < 16; ++j) {
			if( mp_hovered == &mp_data[i][j])
				return i*16 + j;
		}
	}

	return -1;
}

float Level::GetHoveredTileHightlight() const {
	if( mp_hovered == nullptr)
		return -999.9f;

	return mp_hovered->m_highlight;
}

/**
* Creates the mesh data for a plane.
*/
void Level::CreatePlane( float width /*= 1.0f*/, float height /*= 1.0f*/, float depth /*= 1.0f*/, u32 colour /*= 0*/) {
	width /= 2;
	height /= 2;

	mp_verts = new VertPos3fColour4ubNormal3fTex2f[4];
	mp_verts[0] = VertPos3fColour4ubNormal3fTex2f( XMFLOAT3( -width, -height, 0.0f), colour, Vec3YAxis.GetVector(), XMFLOAT2( 0.0f, 1.0f));
	mp_verts[1] = VertPos3fColour4ubNormal3fTex2f( XMFLOAT3( -width,  height, 0.0f), colour, Vec3YAxis.GetVector(), XMFLOAT2( 0.0f, 0.0f));
	mp_verts[2] = VertPos3fColour4ubNormal3fTex2f( XMFLOAT3(  width,  height, 0.0f), colour, Vec3YAxis.GetVector(), XMFLOAT2( 1.0f, 0.0f));
	mp_verts[3] = VertPos3fColour4ubNormal3fTex2f( XMFLOAT3(  width, -height, 0.0f), colour, Vec3YAxis.GetVector(), XMFLOAT2( 1.0f, 1.0f));
	mp_vertBuffer = CreateImmutableVertexBuffer( GET_APP()->mp_D3DDevice, sizeof( VertPos3fColour4ubNormal3fTex2f)*4, mp_verts);

	m_indexCount = 6;
	mp_indicies = new UINT[m_indexCount];
	mp_indicies[0] = 0;
	mp_indicies[1] = 1;
	mp_indicies[2] = 2;
	mp_indicies[3] = 0;
	mp_indicies[4] = 2;
	mp_indicies[5] = 3;
	mp_indexBuffer = CreateImmutableIndexBuffer( GET_APP()->mp_D3DDevice, sizeof( UINT)*m_indexCount, mp_indicies);
}

bool Level::CreateLevelFromFile( const char* fileName) {
	FILE* p_file( nullptr);
	u32 count( 0);
	BITMAPFILEHEADER bmFileHeader;
	BITMAPINFOHEADER bmInfoHeader;
	u8* bmImage( nullptr);
	s32 imageSize( 0);

	// Open the file in binary.
	if( fopen_s( &p_file, fileName, "rb") != 0)
		return false;

	// Read in the file header.
	count = fread( &bmFileHeader, sizeof( BITMAPFILEHEADER), 1, p_file);
	if( count == -1)
		return false;

	// Read in the info header.
	count = fread( &bmInfoHeader, sizeof( BITMAPINFOHEADER), 1, p_file);
	if( count == -1)
		return false;

	// Ensure the image is of the correct dimensions.
	if( bmInfoHeader.biWidth != bmInfoHeader.biHeight)
		return false;

	gsc_levelDimension = bmInfoHeader.biWidth;

	// Calculate the size of the image data and allocate the memory.
	imageSize = bmInfoHeader.biWidth * bmInfoHeader.biHeight * 3;
	bmImage = new u8[imageSize];
	if( bmImage == nullptr)
		return false;

	// Move to the beginning of the bitmap data.
	fseek( p_file, bmFileHeader.bfOffBits, SEEK_SET);

	// Read in the bitmap image data.
	count = fread( bmImage, 1, imageSize, p_file);
	if( count != imageSize)
		return false;

	// Close the file.
	if( fclose( p_file) != 0)
		return false;

	mp_data = new TileData*[gsc_levelDimension];
	for( s32 i( 0); i < gsc_levelDimension; ++i)
		mp_data[i] = new TileData[gsc_levelDimension];

	m_plotCount = 0;
	for( s32 i( 0); i < gsc_levelDimension; ++i) {
		for( s32 j( 0); j < gsc_levelDimension; ++j) {
			s32 pos( (i * gsc_levelDimension * 3) + (j * 3));
			u8 b( bmImage[pos+0]), g( bmImage[pos+1]), r( bmImage[pos+2]);
			TexelCode code( this->DecipherTexel( r, g, b));

			if( code == TC_Plot)
				++m_plotCount;

			mp_data[i][j].m_code = code;

			mp_data[i][j].m_buildingOption = -1;
			mp_data[i][j].m_buildingTime = 0.0f;
			mp_data[i][j].m_built = false;
			mp_data[i][j].m_yeildTime = 0.0f;

			mp_data[i][j].m_offset = Vec3( j - (float(gsc_levelDimension) / 2.0f), 0.0f, i - (float(gsc_levelDimension) / 2.0f));
			mp_data[i][j].m_colour = NumbersToColour( r, g, b, 255);
			mp_data[i][j].m_highlight = 0.0f;
		}
	}

	mp_dataPlots = new TileData*[m_plotCount];
	s32 plotIndex( 0);
	for( s32 i( 0); i < gsc_levelDimension; ++i) {
		for( s32 j( 0); j < gsc_levelDimension; ++j) {
			if( mp_data[i][j].m_code != TC_Plot)
				continue;

			mp_dataPlots[plotIndex] = &mp_data[i][j];
			++plotIndex;
		}
	}

	SafeDeleteArray( bmImage);

	return true;
}

TexelCode Level::DecipherTexel( u8 r, u8 g, u8 b) {
	DBE::Vec3 colour( r, g, b);

	if( colour == Vec3( 255, 0, 0))
		return TC_TownHall;
	else if( colour == Vec3( 0, 255, 0))
		return TC_Grass;
	else if( colour == Vec3( 0, 0, 255))
		return TC_Road;
	//else if( colour == Vec3( 255, 255, 255))
	//	return ;
	else if( colour == Vec3( 0, 0, 0))
		return TC_Plot;

	// If the colour didn't match of the listed combinations that it's invalid.
	return TC_Invalid;
}

/**
* Finds the currently hovered tile and updates the highlight.
*/
void Level::FindAndUpdateHoveredTile() {
	bool found( false);
	for( s32 i( 0); i < gsc_levelDimension; ++i) {
		for( s32 j( 0); j < gsc_levelDimension; ++j) {
			// Skip if it isn't a tile to be checked.
			if( mp_data[i][j].m_code != TC_Plot)
				continue;

			// Skip if the plot is currently building something.
			if( !mp_data[i][j].m_built && mp_data[i][j].m_buildingTime > 0.0f)
				continue;

			// Skip if the plot is already has a building on it.
			if( mp_data[i][j].m_built)
				continue;

			// Move the tile to the correct position and then test for collision.
			this->MoveTo( mp_data[i][j].m_offset);
			if( this->TestForPickedTile()) {
				// If the cursor is over the tile that's already picked, then early out.
				if( mp_hovered == &mp_data[i][j]) {
					found = true;
					break;
				}

				// Reset the old tile's highlight before losing track of it (if it hasn't been picked).
				if( mp_hovered != nullptr) {
					if( mp_hovered != mp_picked)
						mp_hovered->m_highlight = m_highlightMin;
					else
						mp_hovered->m_highlight = m_highlightSelected;
				}

				// Reset the increment to positive.
				if( m_highlightInc < m_highlightMin)
					m_highlightInc *= -1.0f;

				// Save the newly picked tile.
				mp_hovered = &mp_data[i][j];
				found = true;
				break;
			}
		}

		// If the cursor is over a tile then early out.
		if( found)
			break;
	}

	// If the cursor's not over a tile and it was before, then get rid of the old highlight.
	if( !found && mp_hovered != nullptr) {
		if( mp_hovered != mp_picked)
			mp_hovered->m_highlight = m_highlightMin;
		else
			mp_hovered->m_highlight = m_highlightSelected;
		mp_hovered = nullptr;
	}

	// If a tile is picked, then inc/dec the highlight.
	if( mp_hovered != nullptr) {
		mp_hovered->m_highlight += m_highlightInc;

		if( mp_hovered->m_highlight <= m_highlightMin || mp_hovered->m_highlight >= m_highlightMax)
			m_highlightInc *= -1.0f;
	}
}

/**
* Generates resources based on what buildings exist.
*/
void Level::GenerateResources( float deltaTime) {
	// Increment resources based on what's built.
	for( s32 i( 0); i < gsc_levelDimension; ++i) {
		for( s32 j( 0); j < gsc_levelDimension; ++j) {
			// Skip if it isn't a tile to be checked.
			if( mp_data[i][j].m_code != TC_Plot)
				continue;

			// Increment the build time if the plot is currently in development.
			if( !mp_data[i][j].m_built && mp_data[i][j].m_buildingOption != -1) {
				mp_data[i][j].m_buildingTime += deltaTime;

				// If the building has finished building.
				if( mp_data[i][j].m_buildingTime >= Building::GetBuilding( mp_data[i][j].m_buildingOption)->m_buildTime) {
					mp_data[i][j].m_buildingTime = 0.0f;
					mp_data[i][j].m_built = true;
				}

				continue;
			}

			// Skip if the plot either doesn't have a building or it isn't built yet.
			if( !mp_data[i][j].m_built)
				continue;

			mp_data[i][j].m_yeildTime += deltaTime;

			// If it's time to yeild, then produce the resources and reset the yeild time.
			if( mp_data[i][j].m_yeildTime > Building::GetYieldTime()) {
				//m_resources += Building::GetBuilding( mp_data[i][j].m_buildingOption)->m_yield;
				this->AddGold( Building::GetBuilding( mp_data[i][j].m_buildingOption)->m_yield.m_gold);
				
				mp_data[i][j].m_yeildTime -= Building::GetYieldTime();
			}
		}
	}
}

/**
* Updates the objectives.
* NOTE: The way this is implemented it could potentially 'uncheck' a previously completed objective
* if the player later breaks the requirement (i.e. destroys a building, spends gold, etc.).
*/
void Level::UpdateObjectives() {
	// Update the objective tracking.
	u32 count( Building::GetBuildingCount());
	s32* buildingCounts = new s32[count];

	for( u32 i( 0); i < count; ++i)
		buildingCounts[i] = 0;

	// Count how many of each type of building there is.
	for( s32 i( 0); i < m_plotCount; ++i) {
		if( !mp_dataPlots[i]->m_built)
			continue;

		if( mp_dataPlots[i]->m_buildingOption == -1)
			continue;

		++buildingCounts[mp_dataPlots[i]->m_buildingOption];
	}

	// Then pass those values through to the objectives. Externally, it doesn't matter which type
	// of objective they are as internally they ignore the incorrect test so it's only a very small
	// overhead to call both tests for every objective.
	for( s32 i( 0); i < m_objectiveCount; ++i) {
		mp_objectives[i].TestRequirementBuilding( buildingCounts);
		mp_objectives[i].TestRequirementResources( &m_resources);
	}

	SafeDeleteArray( buildingCounts);
}

bool Level::TestForPickedTile() {
	Vec2 mousePos;
	float windowWidth( 0.0f), windowHeight( 0.0f);
	XMFLOAT4X4 p;

	MGR_INPUT().GetMousePos( mousePos);
	windowWidth = (float)GET_APP()->GetWindowWidth();
	windowHeight = (float)GET_APP()->GetWindowHeight();
	DirectX::XMStoreFloat4x4( &p, GET_APP()->m_matProj);

	// Compute picking ray in view space.
	float vx = ( 2.0f * mousePos.GetX() / windowWidth - 1.0f) / p._11;
	float vy = (-2.0f * mousePos.GetY() / windowHeight + 1.0f) / p._22;

	// Ray definition in view space.
	XMVECTOR rayOrigin	= XMVectorSet( 0.0f, 0.0f, 0.0f, 1.0f);
	XMVECTOR rayDir		= XMVectorSet( vx, vy, 1.0f, 0.0f);

	// Transform ray to local space of mesh.
	Matrix4 v = GET_APP()->m_matView;
	Matrix4 invView = XMMatrixInverse( &XMMatrixDeterminant( v), v);

	Matrix4 world = this->GetWorldMatrix();
	Matrix4 invWorld = XMMatrixInverse( &XMMatrixDeterminant( world), world);

	Matrix4 toLocal = XMMatrixMultiply( invView, invWorld);

	rayOrigin = XMVector3TransformCoord( rayOrigin, toLocal);
	rayDir = XMVector3TransformNormal( rayDir, toLocal);

	// Make the ray direction unit length for the intersection tests.
	rayDir = XMVector3Normalize( rayDir);

	// If we hit the bounding box of the mesh, then we might have picked a mesh triangle, so do the
	// ray/triangle tests.
	// If we did not hit the bounding box, then it is impossible that we hit the mesh, so do not
	// waste effort doing ray/triangle tests.

	bool picked( false);
	float tmin( 0.0f);

	// Find the nearest ray/triangle intersection.
	tmin = DBE_Infinity;
	for( UINT i( 0); i < m_indexCount / 3; ++i) {
		// Indicies for this triangle.
		UINT i0 = mp_indicies[i*3+0];
		UINT i1 = mp_indicies[i*3+1];
		UINT i2 = mp_indicies[i*3+2];

		// Verticies for this triangle.
		Vec3 v0 = mp_verts[i0].pos;
		Vec3 v1 = mp_verts[i1].pos;
		Vec3 v2 = mp_verts[i2].pos;

		// We have to iterate over all the triangles in order to find the nearest intersection.
		float t( 0.0f);
		if( Collision::IntersectRayTriangle( Vec3( rayOrigin), Vec3( rayDir), v0, v1, v2, &t)) {
			if( t < tmin) {
				// This is the new nearest picked triangle.
				tmin = t;
				picked = true;
			}
		}
	}

	return picked;
}