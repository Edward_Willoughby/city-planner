/********************************************************************
*	Function definitions for the LevelShader class.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "LevelShader.h"

#include <DBEApp.h>
#include <DBEMath.h>
#include <DBETextureMgr.h>
#include <DBEVertexTypes.h>
/********************************************************************
*	Namespaces, defines, constants, and local variables.
********************************************************************/
using namespace DBE;
using namespace DirectX;


/********************************************************************
*	LevelVertexShader
********************************************************************/
/**
* Constructor.
*/
LevelVertexShader::LevelVertexShader() {}

/**
* Destructor.
*/
LevelVertexShader::~LevelVertexShader() {}

/**
* 
*
* @param :: 
*
* @return 
*/
bool LevelVertexShader::Init() {
	this->LoadShader( "shaders\\LevelShader.hlsl", g_vertPos3fColour4ubNormal3fTex2fMacros, g_vertPos3fColour4ubNormal3fTex2fDesc, g_vertPos3fColour4ubNormal3fTex2fSize);

	this->FindCBuffer( "GlobalVars", &m_slotCBufferGlobalVars);
	this->FindShaderVar( m_slotCBufferGlobalVars, "g_WVP",					SVT_FLOAT4x4,	&m_slotWVP);
	this->FindShaderVar( m_slotCBufferGlobalVars, "g_W",					SVT_FLOAT4x4,	&m_slotLights.m_slotWorld);
	this->FindShaderVar( m_slotCBufferGlobalVars, "g_InvXposeW",			SVT_FLOAT4x4,	&m_slotLights.m_slotInverse);
	this->FindShaderVar( m_slotCBufferGlobalVars, "g_lightDirections",		SVT_FLOAT4,		&m_slotLights.m_slotLightDir);
	this->FindShaderVar( m_slotCBufferGlobalVars, "g_lightPositions",		SVT_FLOAT4,		&m_slotLights.m_slotLightPos);
	this->FindShaderVar( m_slotCBufferGlobalVars, "g_lightColours",			SVT_FLOAT3,		&m_slotLights.m_slotLightColour);
	this->FindShaderVar( m_slotCBufferGlobalVars, "g_lightAttenuations",	SVT_FLOAT4,		&m_slotLights.m_slotLightAtten);
	this->FindShaderVar( m_slotCBufferGlobalVars, "g_lightSpots",			SVT_FLOAT4,		&m_slotLights.m_slotLightSpots);
	this->FindShaderVar( m_slotCBufferGlobalVars, "g_numLights",			SVT_INT,		&m_slotLights.m_slotNumLights);

	if( !this->CreateCBuffer( m_slotCBufferGlobalVars))
		return false;

	return true;
}

/**
* 
*
* @param :: 
*
* @return 
*/
bool LevelVertexShader::SetVars( DBE::Matrix4* world) {
	ID3D11DeviceContext* p_context( GET_APP()->mp_D3DDeviceContext);

	if( mp_CBuffers[m_slotCBufferGlobalVars]) {
		D3D11_MAPPED_SUBRESOURCE map;
		if( !mp_CBuffers[m_slotCBufferGlobalVars] || FAILED( p_context->Map( mp_CBuffers[m_slotCBufferGlobalVars], 0, D3D11_MAP_WRITE_DISCARD, 0, &map)))
			map.pData = nullptr;

		Matrix4 wvp = GET_APP()->GetWVP();

		// Send the WVP matrix to the shader.
		SetCBufferMatrix( map, m_slotWVP, wvp);

		GET_APP()->PassLightsToShader( this, map, &m_slotLights, *world);

		// Do... something to the shader maps.
		if( map.pData)
			p_context->Unmap( mp_CBuffers[m_slotCBufferGlobalVars], 0);
	}

	return true;
}


/********************************************************************
*	LevelPixelShader
********************************************************************/
/**
* Constructor.
*/
LevelPixelShader::LevelPixelShader() {}

/**
* Destructor.
*/
LevelPixelShader::~LevelPixelShader() {}

/**
* 
*
* @param :: 
*
* @return 
*/
bool LevelPixelShader::Init() {
	this->LoadShader( "shaders\\LevelShader.hlsl", g_vertPos3fColour4ubNormal3fTex2fMacros);

	this->FindCBuffer( "GlobalVars", &m_slotCBufferGlobalVars);
	this->FindShaderVar( m_slotCBufferGlobalVars, "g_highlight", SVT_FLOAT, &m_slotHighlight);

	this->FindTexture( "g_texture0", &m_slotTexture0);
	this->FindTexture( "g_texture1", &m_slotTexture1);
	this->FindSampler( "g_sampler", &m_slotSampler);

	this->CreateCBuffer( m_slotCBufferGlobalVars);

	return true;
}

/**
* 
*
* @param :: 
*
* @return 
*/
bool LevelPixelShader::SetVars( float* highlight) {
	ID3D11DeviceContext* p_context( GET_APP()->mp_D3DDeviceContext);

	if( mp_CBuffers[m_slotCBufferGlobalVars]) {
		D3D11_MAPPED_SUBRESOURCE map;
		if( !mp_CBuffers[m_slotCBufferGlobalVars] || FAILED( p_context->Map( mp_CBuffers[m_slotCBufferGlobalVars], 0, D3D11_MAP_WRITE_DISCARD, 0, &map)))
			map.pData = nullptr;

		SetCBufferVar( map, m_slotHighlight, *highlight);

		// Do... something to the shader maps.
		if( map.pData)
			p_context->Unmap( mp_CBuffers[m_slotCBufferGlobalVars], 0);
	}

	return true;
}

/**
* 
*
* @param :: 
*
* @return 
*/
bool LevelPixelShader::SetTexture( DBE::Texture* tex0) {
	ID3D11DeviceContext* p_context( GET_APP()->mp_D3DDeviceContext);

	if( m_slotTexture0 >= 0)
		p_context->PSSetShaderResources( m_slotTexture0, 1, &tex0->mp_textureView);

	//if( m_slotTexture1 >= 0) {
	//	if( tex1 == nullptr)
	//		p_context->PSSetShaderResources( m_slotTexture1, 1, nullptr);
	//	else
	//		p_context->PSSetShaderResources( m_slotTexture1, 1, &tex1->mp_textureView);
	//}

	ID3D11SamplerState* apTextureSampler[] = { tex0->mp_samplerState };
	p_context->PSSetSamplers( m_slotSampler, 1, apTextureSampler);

	return true;
}