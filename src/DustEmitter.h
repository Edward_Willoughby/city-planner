/********************************************************************
*
*	CLASS		:: DustEmitter
*	DESCRIPTION	:: Emitter for dust particles in 'CityPlanner'.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2015 / 03 / 28
*
********************************************************************/

#ifndef DustEmitterH
#define DustEmitterH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <DBEMath.h>
#include <DBEParticle.h>
#include <DBEUITexture.h>

#include "DustParticleShader.h"
/********************************************************************
*	Defines and constants.
********************************************************************/


/*******************************************************************/
class DustParticle : public DBE::Particle {
	public:
		DBE::Vec3 m_rotSpeed;

	private:


};

/*******************************************************************/
class DustParticleMesh : public DBE::UITexture {
	public:
		/// Constructor.
		DustParticleMesh();
		/// Destructor.
		~DustParticleMesh();

		/// Handles some of the rendering that all subsequent particles will need.
		void PreRender( float deltaTime);
		/// Renders the particle (call 'Render').
		void OnRender( float deltaTime);

	private:
		DustParticleVertexShader* mp_dustVS;
		DustParticlePixelShader* mp_dustPS;

		/// Private copy constructor to prevent accidental multiple instances.
		DustParticleMesh( const DustParticleMesh&);
		/// Private assignment operator to prevent accidental multiple instances.
		DustParticleMesh& operator=( const DustParticleMesh&);

};

/*******************************************************************/
class DustEmitter {
	public:
		/// Initialise the particles.
		static bool ParticlesInit();
		/// Deletes the particles.
		static void ParticlesShutdown();

		/// Creates the particles.
		static void ParticlesCreateArray( DBE::Particle** particles, s32 maxParticles);

		/// Event handler for when a particle spawns.
		static void ParticleSpawn( DBE::Particle* particle);
		/// Update a particle.
		static void ParticleUpdate( float deltaTime, DBE::Particle* particle);
		/// Pre-rendering that all particles will require.
		static void ParticlePreRender( float deltaTime);
		/// Render a particle.
		static void ParticleRender( float deltaTime, DBE::Particle* particle, const DBE::Matrix4& parentWorld);

		/// Test if a particle has expired.
		static bool ParticleIsDead( DBE::Particle* particle);
		/// Event handler for when a particle dies.
		static void ParticleDeath( DBE::Particle* particle);
		
	private:
		/// Create a random velocity value.
		static float RandomVelocity();

		static DustParticleMesh* mp_mesh;
		
};

/*******************************************************************/
#endif	// #ifndef DustEmitterH
