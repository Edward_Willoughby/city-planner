/********************************************************************
*
*	CLASS		:: Level
*	DESCRIPTION	:: Loads in a level from a file.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2015 / 03 / 13
*
********************************************************************/

#ifndef LevelH
#define LevelH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <DBEEmitter.h>
#include <DBERenderable.h>
#include <DBETypes.h>

#include "Building.h"
#include "DustEmitter.h"
#include "LevelShader.h"
#include "Objective.h"

namespace DBE {
	class Camera;
	class Vec3;
}
struct ID3D11Buffer;
class LevelTimer;
struct Texture;
struct VertPos3fColour4ubNormal3fTex2f;
/********************************************************************
*	Defines and constants.
********************************************************************/
enum TexelCode {
	TC_Invalid = 0,
	TC_TownHall,	// Red
	TC_Road,		// Blue
	TC_Grass,		// Green
	TC_Plot,		// Black
	TC_Count,
};


/*******************************************************************/
class Level : public DBE::Renderable {
	public:
		/// Constructor.
		Level();
		/// Destructor.
		~Level();
		
		/// Updates the level.
		bool OnUpdate( float deltaTime);
		/// Renders the level.
		void OnRender( float deltaTime);

		/// Loads a level.
		bool LoadLevel( s32 l, DBE::Camera* cam, LevelTimer* timer);
		/// Deletes an existing level.
		void DeleteLevel();

		/// Resets the level to be started again.
		void Reset();

		/// Checks if the player has won this level.
		bool LevelIsWon() const;
		/// Checks if the player has lost this level.
		bool LevelIsLost() const;
		/// Check if a win OR loss condition has been met.
		bool LevelIsOver() const;

		/// Gets the number of objectives for this level.
		s32 GetObjectiveCount() const;
		/// Gets an objective.
		Objective* GetObjective( s32 index) const;

		void AddGold( u32 g);
		void LoseGold( u32 g);
		u32 GetGold() const;

		void PickTile();
		void DeselectTile();

		bool CanAffordBuilding( u32 option);
		void SelectBuilding( u32 option);

		s32 GetHoveredTile() const;
		float GetHoveredTileHightlight() const;
		

	private:
		/**
		* Holds the data for a 'block' in the level.
		*/
		struct TileData {
			// Type of tile.
			TexelCode m_code;

			// For plots.
			s32		m_buildingOption;
			float	m_buildingTime;
			bool	m_built;
			float	m_yeildTime;

			// For rendering.
			DBE::Vec3	m_offset;
			u32			m_colour;
			float		m_highlight;
		};

		/// Creates the plane that's used for rendering the level.
		void CreatePlane( float width = 1.0f, float height = 1.0f, float depth = 1.0f, u32 colour = 0);

		/// Creates tile data from a '.bmp' image.
		bool CreateLevelFromFile( const char* fileName);
		/// Determines the type of tile based on the pixel colour.
		TexelCode DecipherTexel( u8 r, u8 g, u8 b);

		/// Finds the currently hovered tile and updates the highlight.
		void FindAndUpdateHoveredTile();
		/// Generates resources based on what buildings exist.
		void GenerateResources( float deltaTime);
		/// Updates the objectives.
		void UpdateObjectives();
		
		/// Tests to see if the mouse is hovering over a tile.
		bool TestForPickedTile();

		bool m_initialised;

		Resources m_resources;

		Objective*	mp_objectives;
		s32			m_objectiveCount;

		LevelTimer* mp_levelTimer;

		TileData**	mp_data;
		TileData**	mp_dataPlots;
		s32			m_plotCount;
		
		TileData* mp_hovered;
		TileData* mp_picked;

		float m_highlightMin;
		float m_highlightMax;
		float m_highlightInc;
		float m_highlightSelected;

		DBE::Texture* mp_invalid;
		DBE::Texture* mp_grass;
		DBE::Texture* mp_road;
		DBE::Texture* mp_plot;

		DBE::Camera*				mp_activeCam;
		DBE::Emitter<DustEmitter>*	mp_dust;

		LevelVertexShader*	mp_tileVS;
		LevelPixelShader*	mp_tilePS;

		ID3D11Buffer* mp_vertexBuffer;
		ID3D11Buffer* mp_indexBuffer;

		VertPos3fColour4ubNormal3fTex2f*	mp_verts;
		UINT*								mp_indicies;

		UINT m_indexCount;

		
		/// Private copy constructor to prevent accidental multiple instances.
		Level( const Level& other);
		/// Private assignment operator to prevent accidental multiple instances.
		Level& operator=( const Level& other);
		
};

/*******************************************************************/
#endif	// #ifndef LevelH
