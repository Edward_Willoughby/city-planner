/********************************************************************
*	Function definitions for the EndLevelOverlay class.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "EndLevelOverlay.h"

#include <DBEApp.h>
#include <DBEUtilities.h>
/********************************************************************
*	Namespaces, defines, constants, and local variables.
********************************************************************/
using namespace DBE;

static const float gsc_overlayDelay( 1.0f);
static const float gsc_overlayFade( 0.5f);
static const float gsc_overlayFadeDuration( 2.0f);


/**
* Constructor.
*/
EndLevelOverlay::EndLevelOverlay() {
	this->Reset();

	this->MoveTo( 0.0f, 0.0f, -0.5f);

	this->SetTexture( "res/textures/overlay_black.dds", GET_APP()->GetSamplerState( true, true, true));
}

/**
* Destructor.
*/
EndLevelOverlay::~EndLevelOverlay() {
	
}

/**
* Updates the overlay.
*
* @param :: 
*
* @return 
*/
bool EndLevelOverlay::OnUpdate( float deltaTime) {
	if( m_delay < gsc_overlayDelay) {
		m_delay += deltaTime;
	}
	else if( m_fadeDuration < gsc_overlayFadeDuration) {
		m_fadeDuration += deltaTime;
		m_fadeDuration = Clamp<float>( m_fadeDuration, 0.0f, gsc_overlayFadeDuration);

		m_opacity = Lerp( 0.0f, gsc_overlayFade, m_fadeDuration / gsc_overlayFadeDuration);

		u32 colour( this->GetColour());
		SetColourAlpha( colour, u8(m_opacity * 255.0f));
		this->SetColour( colour);
	}
	else {

	}

	return true;
}

///**
//* Renders the overlay.
//*
//* @param :: 
//*/
//void EndLevelOverlay::OnRender( float deltaTime) {
//
//}

/**
* Resets the overlay.
*
* @param :: 
*
* @return 
*/
void EndLevelOverlay::Reset() {
	m_delay = 0.0f;
	m_fadeDuration = 0.0f;
	m_opacity = 0.0f;

	u32 colour( this->GetColour());
	SetColourAlpha( colour, 0);
	this->SetColour( colour);
}