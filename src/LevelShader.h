/********************************************************************
*
*	CLASS		:: LevelShader
*	DESCRIPTION	:: Vertex and pixel shaders for the level.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2015 / 03 / 15
*
********************************************************************/

#ifndef LevelShaderH
#define LevelShaderH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <DBEPixelShader.h>
#include <DBEVertexShader.h>

namespace DirectX {
	struct XMMATRIX;
}
namespace DBE {
	struct Texture;
}
/********************************************************************
*	Defines and constants.
********************************************************************/
/// The unique name for the level's vertex shader so it can be found in the shader manager.
static const char* gsc_levelVertexShaderName = "LevelVertexShader";
/// The unique name for the level's pixel shader so it can be found in the shader manager.
static const char* gsc_levelPixelShaderName = "LevelPixelShader";


/*******************************************************************/
class LevelVertexShader : public DBE::VertexShader {
	public:
		/// Constructor.
		LevelVertexShader();
		/// Destructor.
		~LevelVertexShader();

		bool Init();
		bool SetVars( DBE::Matrix4* world);
		
		s32 m_slotCBufferGlobalVars;

		s32 m_slotWVP;
		ShaderLightingSlots m_slotLights;

	private:
		
		
		/// Private copy constructor to prevent accidental multiple instances.
		LevelVertexShader( const LevelVertexShader& other);
		/// Private assignment operator to prevent accidental multiple instances.
		LevelVertexShader& operator=( const LevelVertexShader& other);
		
};

/*******************************************************************/
class LevelPixelShader : public DBE::PixelShader {
	public:
		/// Constructor.
		LevelPixelShader();
		/// Destructor.
		~LevelPixelShader();
		
		bool Init();
		bool SetVars( float* highlight);
		bool SetTexture( DBE::Texture* tex0);

		s32 m_slotCBufferGlobalVars;
		s32 m_slotHighlight;

		s32 m_slotTexture0;
		s32 m_slotTexture1;
		s32 m_slotSampler;
		
	private:
		
		
		/// Private copy constructor to prevent accidental multiple instances.
		LevelPixelShader( const LevelPixelShader& other);
		/// Private assignment operator to prevent accidental multiple instances.
		LevelPixelShader& operator=( const LevelPixelShader& other);
		
};

/*******************************************************************/
#endif	// #ifndef LevelShaderH
