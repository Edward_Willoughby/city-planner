/********************************************************************
*	Function definitions for the CityPlanner class.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "CityPlanner.h"

#include <DBEAudioMgr.h>
#include <DBECamera.h>
#include <DBECollision.h>
#include <DBEColour.h>
#include <DBEFont.h>

#include "Building.h"
#include "EndLevelOverlay.h"
#include "Objective.h"
/********************************************************************
*	Defines, constants, namespaces, and local variables.
********************************************************************/
using namespace DBE;
using namespace DirectX;

static u32 gs_frames = 0;
static float gs_deltaTotal = 0.0f;

static const char* gsc_sfxBuildingStart( "res/audio/sfx_building_start.mp3");


/**
* Deals with initialisation.
*
* @return True if everything initialised correctly.
*/
bool CityPlanner::HandleInit() {
	mp_cam = new Camera();
	//mp_cam->m_position = Vec3( 0.0f, 10.0f, -10.0f);
	mp_cam->m_position = Vec3( 0.0f, 18.0f, -12.0f);
	mp_cam->m_lookAt = Vec3( 0.0f, 0.0f, -3.0f);

	Building::Init();

	mp_levelTimer = new LevelTimer();
	mp_levelTimer->Init();

	mp_level = new Level();
	if( !mp_level->LoadLevel( 0, mp_cam, mp_levelTimer))
		return false;

	mp_ui = new UserInterface();
	if( !mp_ui->Init())
		return false;

	mp_overlay = new EndLevelOverlay();

	Font::InstallFont( "res/fonts/TimeGoesBySoSlowly.ttf");
	mp_UIFont = Font::CreateByName( "FontTimeGoesBySoSlowly", "Time goes by so slowly", 20, 0);

	m_state = GameState::GS_Play;

	MGR_AUDIO().SetMasterVolume( 0.3f);

	this->EnableLightDirectional( 0, Vec3( -1.0f, -1.0f, -1.0f), Vec3( 1.0f, 1.0f, 1.0f)); //0.75f, 0.75f, 0.85f));

	m_wireframe = false;

	m_windowTitle = "City Planner";
	this->SetWindowTitle( m_windowTitle);

	this->SetFocusResponse( FocusResponse::FR_SlowRendering);

	return true;
}

/**
* Deals with any input.
*
* @return True if nothing went wrong.
*/
bool CityPlanner::HandleInput() {
	// Toggle wireframe mode.
	if( MGR_INPUT().KeyPressed( 'W'))
		m_wireframe = !m_wireframe;

	// Quit the program.
	if( MGR_INPUT().KeyPressed( VK_ESCAPE))
		this->Terminate();

	static bool s_capFPS( true);
	if( MGR_INPUT().KeyPressed( 'F')) {
		s_capFPS = !s_capFPS;

		if( s_capFPS)
			FRAME_TIMER().SetFPS( true, 60);
		else
			FRAME_TIMER().SetFPS( false);
	}

	static float mov( 0.1f);
	//if( MGR_INPUT().KeyHeld( 'W'))
	//	mp_cube->MoveBy( mov, 0.0f, 0.0f);
	//if( MGR_INPUT().KeyHeld( 'S'))
	//	mp_cube->MoveBy( -mov, 0.0f, 0.0f);
	//if( MGR_INPUT().KeyHeld( 'A'))
	//	mp_cube->MoveBy( 0.0f, 0.0f, mov);
	//if( MGR_INPUT().KeyHeld( 'D'))
	//	mp_cube->MoveBy( 0.0f, 0.0f, -mov);

	//if( MGR_INPUT().KeyPressed( 'Q'))
	//	mp_ui->ShowOptions( OL_Buildings);
	//else if( MGR_INPUT().KeyPressed( 'W'))
	//	mp_ui->ShowOptions( OL_LevelWon);
	//else if( MGR_INPUT().KeyPressed( 'E'))
	//	mp_ui->ShowOptions( OL_LevelLost);


	return true;
}

/**
* Deals with any updating.
*
* @param deltaTime :: The time taken to render the last frame.
*
* @return True if everything updated correctly.
*/
bool CityPlanner::HandleUpdate( float deltaTime) {
	// Set the window title.
	char buffer[256];
	sprintf_s( buffer, sizeof( buffer), "%s - FPS: %f, Delta: %f", m_windowTitle, FRAME_TIMER().FPS(), FRAME_TIMER().DeltaTime());
	this->SetWindowTitle( buffer);

	switch( m_state) {
		case GS_Play:
			if( MGR_INPUT().KeyPressed( VK_LBUTTON)) {
				if( mp_ui->GetSelectedOption() >= 0) {
					if( mp_level->CanAffordBuilding( mp_ui->GetSelectedOption())) {
						// Build that building.
						mp_level->SelectBuilding( mp_ui->GetSelectedOption());
						mp_ui->ShowOptions( OL_None);

						// Play the sound effect.
						MGR_AUDIO().PlayAudioOneShot( gsc_sfxBuildingStart);
						
						// Start the timer.
						mp_levelTimer->Start();
					}
				}
				else if( mp_level->GetHoveredTile() >= 0) {
					mp_level->PickTile();
					mp_ui->ShowOptions( OL_Buildings);
				}
				else {
					mp_level->DeselectTile();
					mp_ui->ShowOptions( OL_None);
				}
			}

			mp_level->Update( deltaTime);
			mp_ui->Update( deltaTime);

			if( mp_level->LevelIsOver()) {
				mp_levelTimer->Pause();

				DebugTrace( "Time bonus: %i\n", mp_levelTimer->GetBonus());

				if( mp_level->LevelIsWon()) {
					m_state = GameState::GS_LevelComplete;
					mp_ui->ShowOptions( OL_LevelWon);
				}
				else if( mp_level->LevelIsLost()) {
					m_state = GameState::GS_LevelFailed;
					mp_ui->ShowOptions( OL_LevelLost);
				}
			}
			break;

		case GS_LevelComplete:
			if( MGR_INPUT().KeyPressed( VK_LBUTTON)) {
				s32 option( mp_ui->GetSelectedOption());

				if( option >= 0) {
					if( option == 0) {
						this->Terminate();
					}
					//else if( option == 1) {

					//}
				}
			}

			mp_overlay->Update( deltaTime);
			mp_level->Update( deltaTime);
			mp_ui->Update( deltaTime);
			break;

		case GS_LevelFailed:
			if( MGR_INPUT().KeyPressed( VK_LBUTTON)) {
				s32 option( mp_ui->GetSelectedOption());

				if( option >= 0) {
					if( option == 0) {
						// Reset the level.
						m_state = GS_Play;
						mp_level->Reset();
						mp_ui->Reset();
						mp_overlay->Reset();
					}
					else if( option == 1) {
						this->Terminate();
					}
				}
			}

			mp_overlay->Update( deltaTime);
			mp_level->Update( deltaTime);
			mp_ui->Update( deltaTime);
			break;
	}

	mp_cam->Update();

	++gs_frames;
	gs_deltaTotal += deltaTime;

	return true;
}

/**
* Deals with any rendering.
*
* @param deltaTime :: The time taken to render the last frame.
*
* @return True if everything rendered correctly.
*/
bool CityPlanner::HandleRender( float deltaTime) {
	this->HandleRender3D( deltaTime);
	this->HandleRender2D( deltaTime);

	// Re-apply the 3D projection matrix and view matrix so they're stored for the next update and
	// can thus be used for 'picking'.
	mp_cam->ApplyPerspectiveMatrixLH( 1.0f, 1000.0f);
	mp_cam->ApplyViewMatrixLH();

	return true;
}

/**
* Deals with any 2D rendering.
*
* @param deltaTime :: The time taken to render the last frame.
*/
void CityPlanner::HandleRender2D( float deltaTime) {
	mp_cam->ApplyOrthoMatrixLH( 1.0f, 1000.0f);

	GET_APP()->SetBlendState( true);
	GET_APP()->SetDepthStencilState( true, true);
	GET_APP()->SetRasteriserState( false, m_wireframe);

	// Render the player's current resources.
	mp_UIFont->DrawStringf2D( Vec3( 0.40f, 0.45f, 0.0f), Vec3( 0.0f), 1.0f, &Font::Style( GOLD), "%ig", mp_level->GetGold());

	// Render the current level's objectives.
	for( s32 i( 0); i < mp_level->GetObjectiveCount(); ++i) {
		Objective* p_obj( mp_level->GetObjective( i));
		Font::Style style;

		if( p_obj->IsComplete())
			style = Font::Style( LIGHT_GREY);

		mp_UIFont->DrawString2D( Vec3( -0.5f, 0.4f - (i * 0.05f), 0.0f), Vec3( 0.0f), 1.0f, &style, p_obj->GetDescription());
	}

	mp_levelTimer->Render( deltaTime);

	mp_ui->Render( deltaTime);

	switch( m_state) {
		case GS_Play:
			// Render the details of the currently hovered building.
			if( mp_ui->GetSelectedOption() != -1) {
				Building* p_building( Building::GetBuilding( mp_ui->GetSelectedOption()));
				float scale( 1.0f);

				// Name.
				mp_UIFont->DrawStringf2D( Vec3( -0.43f, -0.40f, 0.0f), Vec3( 0.0f), 1.0f, nullptr, "%s", p_building->m_name);

				// Cost.
				mp_UIFont->DrawString2D( Vec3( -0.45f, -0.45f, 0.0f), Vec3( 0.0f), scale, nullptr, "Cost:");
				mp_UIFont->DrawStringf2D( Vec3( -0.35f, -0.45f, 0.0f), Vec3( 0.0f), scale, nullptr, "%ig", p_building->m_cost.m_gold);

				// Yield.
				mp_UIFont->DrawString2D( Vec3( -0.45f, -0.50f, 0.0f), Vec3( 0.0f), scale, nullptr, "Yield:");
				mp_UIFont->DrawStringf2D( Vec3( -0.35f, -0.50f, 0.0f), Vec3( 0.0f), scale, nullptr, "%ig", p_building->m_yield.m_gold);
			}
			break;

		case GS_LevelComplete:
			mp_overlay->Render( deltaTime);
			mp_UIFont->DrawString2D( Vec3( -0.1f, 0.0f, -1.0f), Vec3( 0.0f), 2.0f, nullptr, "Level Complete!");

			char buffer[256];
			TimeBonus bonus( mp_levelTimer->GetBonus());
			if( bonus == TimeBonus::TB_None)
				sprintf_s( buffer, 256, "No bonus awarded.");
			else
				sprintf_s( buffer, 256, "Level %i bonus awarded!", bonus + 1);

			mp_UIFont->DrawString2D( Vec3( -0.12f, -0.05f, -1.0f), Vec3( 0.0f), 2.0f, nullptr, buffer);
			break;
	}

	// Debug text.
	//DEFAULT_FONT()->DrawStringf2D( Vec3( -0.5f, 0.275f, 0.0f), Vec3( 0.0f), 1.0f, nullptr, "Picked: %i", mp_level->GetHoveredTile());
	//DEFAULT_FONT()->DrawStringf2D( Vec3( -0.5f, 0.250f, 0.0f), Vec3( 0.0f), 1.0f, nullptr, "Highlight: %f", mp_level->GetHoveredTileHightlight());
	//DEFAULT_FONT()->DrawStringf2D( Vec3( -0.5f, 0.225f, 0.0f), Vec3( 0.0f), 1.0f, nullptr, "Building Hovered: %i", mp_ui->GetSelectedOption());
	
	//Vec2 pos, screen;
	//if( MGR_INPUT().GetMousePos( pos)) {
	//	if( pos.GetX() != -1 && pos.GetY() != -1) {
	//		screen.SetX( (pos.GetX() / SCREEN_WIDTH) - 0.5f);
	//		screen.SetY( ((pos.GetY() / SCREEN_HEIGHT) - 0.5f) * -1.0f);
	//	}
	//}
	//DEFAULT_FONT()->DrawStringf2D( Vec3( -0.5f, 0.125f, 0.0f), Vec3( 0.0f), 1.0f, nullptr, "X: %f, Y: %f\n", pos.GetX(), pos.GetY());
	//DEFAULT_FONT()->DrawStringf2D( Vec3( -0.5f, 0.100f, 0.0f), Vec3( 0.0f), 1.0f, nullptr, "X: %f, Y: %f\n", screen.GetX(), screen.GetY());
}

/**
* Deals with any 3D rendering.
*
* @param deltaTime :: The time taken to render the last frame.
*/
void CityPlanner::HandleRender3D( float deltaTime) {
	mp_cam->ApplyPerspectiveMatrixLH( 1.0f, 1000.0f);
	mp_cam->ApplyViewMatrixLH();

	this->SetBlendState( true);
	this->SetDepthStencilState( true, true);
	this->SetRasteriserState( false, m_wireframe);

	mp_level->Render( deltaTime);
}

/**
* Deals with anything that needs to be released or shutdown.
*
* @return True if everything was closed correctly.
*/
bool CityPlanner::HandleShutdown() {
	mp_ui->Shutdown();
	Building::Shutdown();
	mp_levelTimer->Shutdown();

	SafeDelete( mp_level);
	SafeDelete( mp_levelTimer);
	SafeDelete( mp_ui);
	SafeDelete( mp_UIFont);

	SafeDelete( mp_overlay);

	DebugTrace( "Frames: %i\nDelta Average: %f\nAverage F.P.S.: %f\n", gs_frames, gs_deltaTotal / gs_frames, gs_frames / gs_deltaTotal);

	return true;
}