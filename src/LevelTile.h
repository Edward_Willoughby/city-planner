/********************************************************************
*
*	CLASS		:: LevelTile
*	DESCRIPTION	:: Essentially a 'BasicMesh' with an overloaded render function so it can render all
*					of the level's tiles using the same rendering data to make it a bit more
*					efficient.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2015 / 03 / 15
*
********************************************************************/

#ifndef LevelTileH
#define LevelTileH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <DBEBasicMesh.h>

/********************************************************************
*	Defines and constants.
********************************************************************/


/*******************************************************************/
class LevelTile : public DBE::BasicMesh {
	public:
		/// Constructor.
		LevelTile();
		/// Destructor.
		~LevelTile();
		
		//void OnRender( float deltaTime);
		
	private:
		
		
		/// Private copy constructor to prevent accidental multiple instances.
		LevelTile( const LevelTile& other);
		/// Private assignment operator to prevent accidental multiple instances.
		LevelTile& operator=( const LevelTile& other);
		
};

/*******************************************************************/
#endif	// #ifndef LevelTileH
